package cz.fel.cvut.ts1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    private static WebDriver driver;



    @BeforeAll
    static void setup() {
        driver = new ChromeDriver();
    }
    @ParameterizedTest
    @CsvSource({"kosticinvitalij07@gmail.com , 112233445566778899"})
    void login(String email, String password) {

        driver.get("https://link.springer.com");


        driver.findElement(By.xpath("/html/body/dialog/div[2]/div/div[2]/button[1]")).click();

        driver.findElement(By.xpath("//*[@id=\"identity-account-widget\"]")).click();


        driver.findElement(By.xpath("/html/body/section/div/div[2]/button[1]")).click();

        driver.findElement(By.xpath("//*[@id=\"login-email\"]")).sendKeys(email);

        driver.findElement(By.xpath("//*[@id=\"email-submit\"]")).click();

        driver.findElement(By.xpath("//*[@id=\"login-password\"]")).sendKeys(password);

        driver.findElement(By.xpath("//*[@id=\"password-submit\"]")).click();


    }

    @Test
    void advanced_search() {


        driver.get("https://link.springer.com/advanced-search");

        driver.findElement(By.xpath("//*[@id=\"all-words\"]")).sendKeys("Page Object Model");

        driver.findElement(By.xpath("//*[@id=\"least-words\"]")).sendKeys("Sellenium Testing");

        driver.findElement(By.xpath("//*[@id=\"facet-start-year\"]")).sendKeys("2024");

        driver.findElement(By.xpath("//*[@id=\"facet-end-year\"]")).sendKeys("2024");

        driver.findElement(By.xpath("//*[@id=\"submit-advanced-search\"]")).click();


        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));


        WebElement labelArticle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div/details/div/ol/li[2]/div/label")));

        
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", labelArticle);


        driver.findElement(By.xpath("//*[@id=\"popup-filters\"]/div[3]/button[2]")).click();

        String Name;
        String Date;
        String DOI;
        ArrayList<String> Names = new ArrayList<>(4);
        ArrayList<String> Dates = new ArrayList<>(4);
        ArrayList<String> DOIs = new ArrayList<>(4);

        for (int i = 1; i <= 4; i++) {

            Name = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/h3/a")).getText();

            DOI = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/h3/a")).getAttribute("href");
            if (i == 4) {
                driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
                Date = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div[2]/ol/li[4]/div/div/div[3]/div/span[3]")).getText();

            } else {

                Date = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/div[3]/div/span[2]")).getText();
            }

            DOI = DOI.substring(34);

            Names.add(Name);
            Dates.add(Date);
            DOIs.add(DOI);
        }


        for (int i = 1; i <= 4; i++) {
            driver.get("https://link.springer.com/search");

            driver.findElement(By.xpath("//*[@id=\"search-springerlink\"]")).sendKeys(Names.get(i - 1));

            driver.findElement(By.xpath("//*[@id=\"search-submit\"]")).click();


            driver.findElement(By.xpath("//*[@id=\"popup-filters\"]/div[3]/button[2]")).click();
            Assertions.assertEquals(DOIs.get(i - 1), driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/h3/a")).getAttribute("href").substring(34));

            if (i == 4) {
                Assertions.assertEquals(Dates.get(i - 1), driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div/div/div[3]/div/span[3]")).getText());
            } else {
                Assertions.assertEquals(Dates.get(i - 1), driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/div[3]/div/span[2]")).getText());
            }


        }

        driver.quit();

    }

}





